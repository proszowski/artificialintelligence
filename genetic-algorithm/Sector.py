class Sector:
    def __init__(self, candidate, total_goodness):
        self.candidate = candidate
        self.size = float(candidate.goodness) / float(total_goodness)
