import random

import math

from Candidate import Candidate
from Mutator import Mutator
from Pair import Pair
from Roulette import Roulette

N = 7
EXPECTED_GOODNESS = 2 * (127 ** 2 + 1)


def main():
    initial_population = generate_initial_population()

    good_enough = False

    iteration = 0

    while not good_enough:
        iteration = iteration + 1
        roulette = Roulette(initial_population)
        next_population = roulette.choose_next_population()
        pairs_to_mutate = pick_pairs_to_mutate(next_population)
        pairs_after_mutation = mutate(pairs_to_mutate)
        current_population = get_current_population(pairs_after_mutation)
        best_candidate = get_best_candidate(current_population)
        good_enough = is_good_enough(best_candidate)
        initial_population = current_population
        if iteration > 10000:
            print('Algorithm stuck and it won\'t achieve better result.')
            break

    print('Best candidate ' + str(best_candidate))


def get_best_candidate(current_population):
    best_candidate = current_population[0]
    for candidate in current_population:
        if candidate.goodness > best_candidate.goodness:
            best_candidate = candidate
    return best_candidate


def is_good_enough(candidate):
    return candidate.goodness >= EXPECTED_GOODNESS


def get_current_population(pairs_after_mutation) -> []:
    current_population = []
    for pair in pairs_after_mutation:
        current_population.append(pair.first)
        current_population.append(pair.second)
    return current_population


def mutate(pairs_to_mutate) -> []:
    mutated_pairs = []
    mutator = Mutator()
    for pair in pairs_to_mutate:
        mutated_pairs.append(mutator.cross(pair))
    return mutated_pairs


def pick_pairs_to_mutate(next_population):
    pairs_to_mutate = []
    for i in range(0, math.floor(len(next_population) * 0.5)):
        pairs_to_mutate.append(Pair.random_from(next_population))
    return pairs_to_mutate


def generate_initial_population() -> []:
    initial_population = []
    for i in range(0, 10):
        c = Candidate(random.randint(1, pow(2, N) - 1), N)
        initial_population.append(c)
    return initial_population


if __name__ == '__main__':
    main()
