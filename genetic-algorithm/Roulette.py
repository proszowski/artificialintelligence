import random

from Sector import Sector


class Roulette:
    def __init__(self, population):
        self.population = population
        self.total = self._calculate_total()
        self.sectors = self._calculate_sectors()

    def _calculate_total(self):
        total = 0
        for candidate in self.population:
            total += candidate.goodness
        return total

    def _calculate_sectors(self):
        sectors = []
        for candidate in self.population:
            sectors.append(Sector(candidate, self.total))
        return sectors

    def choose_next_population(self):
        next_population = []
        for i in range(0, len(self.sectors)):
            pointer = random.random()
            sector = self._find_right_sector(pointer)
            next_population.append(sector.candidate)
        return next_population

    def _find_right_sector(self, pointer):
        total = 0
        for sector in self.sectors:
            total += sector.size
            if total >= pointer:
                return sector
        return self.sectors[-1]
