
class Candidate:
    def __init__(self, fenotype, n):
        self.fenotype = fenotype
        self.chromosome = Candidate._get_bin(self.fenotype, n)
        self.goodness = 2 * (pow(fenotype, 2) + 1)

    @staticmethod
    def from_chromosome(chromosome):
        n = len(chromosome)
        fenotype = Candidate._get_dec(chromosome)
        return Candidate(fenotype, n)

    def __str__(self):
        return 'fenotype: ' + str(self.fenotype) + ', goodness: ' + str(self.goodness) + ', chromosome: ' + self.chromosome

    @staticmethod
    def _get_bin(fenotype, n):
        return format(fenotype, 'b').zfill(n)

    @staticmethod
    def _get_dec(chromosome):
        return int(chromosome, 2)

