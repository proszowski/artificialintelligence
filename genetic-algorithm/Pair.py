import random


class Pair:
    def __init__(self, first, second):
        self.first = first
        self.second = second

    @staticmethod
    def random_from(candidates):
        first = random.choice(candidates)
        second = random.choice(candidates)
        return Pair(first, second)
