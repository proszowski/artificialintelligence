import random

from Candidate import Candidate
from Pair import Pair


class Mutator:
    def cross(self, pair) -> Pair:
        locus = Mutator._get_locus(pair)
        first = pair.first.chromosome
        second = pair.second.chromosome
        first_chromosome = first[:locus] + second[locus:]
        second_chromosome = second[:locus] + first[locus:]
        first_candidate = Candidate.from_chromosome(first_chromosome)
        second_candidate = Candidate.from_chromosome(second_chromosome)
        return Pair(first_candidate, second_candidate)

    @staticmethod
    def _get_locus(pair) -> int:
        length = len(pair.first.chromosome)
        locus = random.randint(1, length)
        return locus
