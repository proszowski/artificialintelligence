from DistanceBetweenNodesCalculator import DistanceBetweenNodesCalculator


class Path:

    def __init__(self):
        self.nodes = []
        self.cost = 0

    def addNode(self, node):
        self._updateCost(node)
        self.nodes.append(node)

    def _updateCost(self, node):
        if len(self.nodes) > 0:
            previousNode = self.nodes[-1]
            self.cost += self._calculateDistanceBetween(previousNode, node)
        else:
            pass;

    def _calculateDistanceBetween(self, previousNode, node):
        return DistanceBetweenNodesCalculator.calculate(previousNode, node)

    def copy(self):
        path = Path()
        path.nodes = self.nodes.copy()
        path.cost = self.cost
        return path

    def __str__(self):
        message = "Path({ nodes:[ "
        for node in self.nodes:
            message += str(node.id) + " "
        message += "]\n cost: " + str(self.cost) + " })"
        return message

