class Node:
    def __init__(self, id, x, y):
        self.id = id
        self.x = x
        self.y = y

    def __str__(self):
        return "{id: " + str(self.id) + ", x: " + str(self.x) + ", y: " + str(self.y) + "}"
