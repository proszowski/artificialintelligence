from Path import Path
from TSPSolver import TSPSolver


class TSPBruteForceSolver(TSPSolver):

    def __init__(self):
        self.possiblePaths = []

    def solve(self, nodes) -> Path:
        super().solve(nodes)
        self.possiblePaths = []
        for node in nodes:
            path = Path()
            path.addNode(node)
            self._generateEveryPossiblePath(path, nodes)
        return self._getShortestPath()

    def _generateEveryPossiblePath(self, path, nodes):
        if (len(nodes) == 1):
            self.possiblePaths.append(path)
        else:
            remainingNodes = [node for node in nodes if node not in path.nodes]
            for node in remainingNodes:
                newPath = path.copy()
                newPath.addNode(node)
                self._generateEveryPossiblePath(newPath, remainingNodes)

    def _getShortestPath(self):
        return sorted(self.possiblePaths, key=lambda x: x.cost, reverse=False)[0]


