class PathWithDuration():

    def __init__(self, path, duration):
        self.path = path
        self.duration = duration

    def __str__(self):
        return self.path.__str__()
