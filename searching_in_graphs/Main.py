from NodeFactory import NodeFactory
from PathWithDuration import PathWithDuration
from TSPSolverFactory import TSPSolverFactory
from TSPSolverWithDuration import TSPSolverWithDuration
import numpy as np
import pandas as pd

BREADTH_FIRST_SEARCH = "Breadth First Search Algorithm"
NEAREST_NEIGHBOUR = "Nearest Neighbour Algorithm"
RANDOM_SWAP = "Random Swap Algorithm"


def solveTSP(TSPSolver, name, nodes):
    solver = TSPSolverWithDuration(TSPSolver, name)
    pathWithDuration = solver.solve(nodes)
    print("Duration of " + name + ": " + str(pathWithDuration.duration))
    print("The best path for " + name + ": \n" + pathWithDuration.__str__() + "\n")
    return pathWithDuration


def printNodes(nodes):
    for node in nodes:
        print(node.__str__())
    print("\n")


def getAverageResult(results, name):
    totalCost = 0
    totalDuration = 0

    for pathWithDuration in results:
        totalCost += pathWithDuration.path.cost
        totalDuration += pathWithDuration.duration.microseconds

    averageCost = totalCost / len(results)
    averageDuration = totalDuration / len(results)

    print("Average results for " + name + ":\n")
    print("Average cost: " + str(averageCost))
    print("Average duration: " + str(averageDuration) + "ms")
    print("\n")
    return PathWithDuration(averageCost, averageDuration)


def getAverageResults(numberOfNodes, bruteForceResults, neareastNeighbourResults, randomSwapResults):
    print("=========AVERAGE RESULTS FOR " + str(numberOfNodes) + " NODES==========")
    first = getAverageResult(bruteForceResults, BREADTH_FIRST_SEARCH)
    second = getAverageResult(neareastNeighbourResults, NEAREST_NEIGHBOUR)
    third = getAverageResult(randomSwapResults, RANDOM_SWAP)
    return [first, second, third]


def main():
    minNumberOfNodes = 4
    maxNumberOfNodes = 11
    numberOfIterations = 10


    costAverageSeries = []
    timeAverageSeries = []
    for numberOfNodes in range(minNumberOfNodes, maxNumberOfNodes):
        bruteForceResults = []
        neareastNeighbourResults = []
        randomSwapResults = []
        for i in range(0, numberOfIterations):
            print("\nNumber of nodes: " + str(numberOfNodes) + "\n")
            nodes = NodeFactory.getNRandomNodes(numberOfNodes)
            printNodes(nodes)

            bruteForceResult = solveTSP(TSPSolverFactory.bruteForceSolver(), BREADTH_FIRST_SEARCH, nodes)
            neareastNeighbourResult = solveTSP(TSPSolverFactory.nearestNeighbourSolver(), NEAREST_NEIGHBOUR, nodes)
            randomSwapResult = solveTSP(TSPSolverFactory.randomSwapSolver(), RANDOM_SWAP, nodes)

            bruteForceResults.append(bruteForceResult)
            neareastNeighbourResults.append(neareastNeighbourResult)
            randomSwapResults.append(randomSwapResult)

        serie = getAverageResults(numberOfNodes, bruteForceResults, neareastNeighbourResults, randomSwapResults)
        costAverageSeries.append(map(lambda x: x.path, serie))
        timeAverageSeries.append(map(lambda x: x.duration, serie))
    costFrame = pd.DataFrame(index=np.arange(minNumberOfNodes, maxNumberOfNodes), data=costAverageSeries)
    timeFrame = pd.DataFrame(index=np.arange(minNumberOfNodes, maxNumberOfNodes), data=timeAverageSeries)
    print(costFrame)
    print(timeFrame)


if __name__ == "__main__":
    main()
