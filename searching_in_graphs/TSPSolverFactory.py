from TSPRandomSwapSolver import TSPRandomSwapSolver
from TSPSolver import TSPSolver
from TSPBruteForceSolver import TSPBruteForceSolver
from TSPNearestNeighbourSolver import TSPNearestNeighbourSolver


class TSPSolverFactory:

    @staticmethod
    def bruteForceSolver() -> TSPSolver:
        return TSPBruteForceSolver()

    @staticmethod
    def nearestNeighbourSolver() -> TSPSolver:
        return TSPNearestNeighbourSolver()

    @staticmethod
    def randomSwapSolver() -> TSPSolver:
        return TSPRandomSwapSolver()


