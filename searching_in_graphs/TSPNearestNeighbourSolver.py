from DistanceBetweenNodesCalculator import DistanceBetweenNodesCalculator
from Path import Path
from TSPSolver import TSPSolver


class TSPNearestNeighbourSolver(TSPSolver):

    def solve(self, nodes) -> Path:
        super().solve(nodes)
        self.possiblePaths = []
        if len(nodes) < 1:
            return Path()
        remainingNodes = nodes.copy()
        path = Path()
        node = nodes[0]
        path.addNode(nodes[0])
        remainingNodes.remove(node)
        while len(remainingNodes) > 0:
            node = self._getNearestNodeFor(path.nodes[-1], remainingNodes)
            path.addNode(node)
            remainingNodes.remove(node)
        return path

    def _getNearestNodeFor(self, node, remainingNodes):
        currentNearestNode = remainingNodes[0]
        currentLowestCost = DistanceBetweenNodesCalculator.calculate(node, remainingNodes[0])
        for i in range(len(remainingNodes) - 1):
            currentNode = remainingNodes[i+1]
            cost = DistanceBetweenNodesCalculator.calculate(node, currentNode)
            if cost < currentLowestCost:
                currentNearestNode = currentNode
                currentLowestCost = cost
        return currentNearestNode


