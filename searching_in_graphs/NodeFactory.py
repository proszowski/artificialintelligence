import random

from Node import Node


class NodeFactory:

    @staticmethod
    def getRandomNode(id) -> Node:
        x = random.random() * 100
        y = random.random() * 100
        return Node(id, x, y)

    @staticmethod
    def getNRandomNodes(n) -> list:
        nodes = []
        for i in range(n):
            nodes.append(NodeFactory.getRandomNode(i + 1))
        return nodes
