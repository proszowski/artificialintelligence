import math


class DistanceBetweenNodesCalculator:
    @staticmethod
    def calculate(a, b):
        return math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)



