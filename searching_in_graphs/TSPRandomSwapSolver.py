import random

from Path import Path
from TSPSolver import TSPSolver



class TSPRandomSwapSolver(TSPSolver):

    def __init__(self):
        self.currentPath = Path()
        self.currentIndexesOrder = []

    def solve(self, nodes) -> Path:
        self.howManyIterations = len(nodes)**3
        self.nodes = nodes
        self.createInitialPath()
        for iteration in range(self.howManyIterations):
            self.makeRandomSwap()

        return self.currentPath

    def createInitialPath(self):
        for node in self.nodes:
            self.currentPath.addNode(node)

        for i in range(len(self.nodes)):
            self.currentIndexesOrder.append(i + 1)

    def makeRandomSwap(self):
        firstIndex = random.randint(1,len(self.currentPath.nodes) - 1)
        secondIndex = random.randint(1,len(self.currentPath.nodes) - 1)
        alternativeIndexesOrder = self.currentIndexesOrder.copy()
        tmp = alternativeIndexesOrder[firstIndex]
        alternativeIndexesOrder[firstIndex] = alternativeIndexesOrder[secondIndex]
        alternativeIndexesOrder[secondIndex] = tmp
        alternativePath = Path()
        for x in alternativeIndexesOrder:
            alternativePath.addNode(self.nodes[x - 1])

        if alternativePath.cost < self.currentPath.cost:
            self.currentPath = alternativePath
            self.currentIndexesOrder = alternativeIndexesOrder
