from multiprocessing.pool import ThreadPool
from datetime import datetime

from PathWithDuration import PathWithDuration


class TSPSolverWithDuration:

    def __init__(self, solver, name):
        self.solver = solver
        self.pool = ThreadPool(processes=1)
        self.name = name

    def solve(self, nodes) -> PathWithDuration:
        startTime = datetime.now()
        result = self.pool.apply_async(self.solver.solve, args=(nodes,))
        while True:
            if result.ready():
                duration = self.calculateDuration(startTime)
                return PathWithDuration(result.get(), duration)

    def calculateDuration(self, startTime):
        return datetime.now() - startTime
