class PointWithCategory:
    def __init__(self, point, category):
        self.point = point
        self.category = category

    def __str__(self) -> str:
        return "PointWithCategory(" + str(self.point) + ", " + str(self.category) + ")"


