import random

from Point import Point
from PointWithCategory import PointWithCategory


class PointsWithCategoryFactory:
    @staticmethod
    def get_n_points(how_many_points, category, min_constraint_point, max_constraint_point) -> []:
        points = []
        for i in range(0, how_many_points):
            x = random.randint(min_constraint_point.x, max_constraint_point.x)
            y = random.randint(min_constraint_point.y, max_constraint_point.y)
            points.append(PointWithCategory(Point(x, y), category))

        return points
