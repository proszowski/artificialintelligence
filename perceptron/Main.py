import random
import matplotlib.pyplot as plt
import numpy as np

from Category import Category
from Point import Point
from PointsFactory import PointsFactory
from PointsWithCategoryFactory import PointsWithCategoryFactory


def update_weights(weights, error, point) -> []:
    new_weights = weights + error * np.matrix([point.x, point.y]).transpose()
    return new_weights


def add_test_data_to_the_plot(green_circle, test_data):
    plt.plot([point.x for point in test_data], [point.y for point in test_data], green_circle)


def add_training_data_to_the_plot(blue_circle, points_a, points_b, red_circle):
    plt.plot([point.point.x for point in points_a], [point.point.y for point in points_a], red_circle)
    plt.plot([point.point.x for point in points_b], [point.point.y for point in points_b], blue_circle)
    plt.axis([-100, 100, -100, 100])


def main():
    points_a = PointsWithCategoryFactory.get_n_points(15, Category.A, Point(-100, -100), Point(0, 0))
    points_b = PointsWithCategoryFactory.get_n_points(15, Category.B, Point(0, 0), Point(100, 100))

    red_circle = 'ro'
    blue_circle = 'bo'
    green_circle = 'co'
    red_cross = 'rx'
    blue_cross = 'bx'

    add_training_data_to_the_plot(blue_circle, points_a, points_b, red_circle)

    weights = np.matrix([random.random(), random.random()]).transpose()
    bias = random.random()
    print("Initial weights: " + str(weights))
    print("Initial bias: " + str(bias))

    training_data = np.concatenate([points_a, points_b])

    total_error = 0
    number_of_epochs = 0
    while total_error == 0:
        number_of_epochs += 1
        total_error = 0
        for record in training_data:
            prediction = np.matrix([record.point.x, record.point.y]) * weights + bias
            y = 1 if prediction > 0 else 0
            error = record.category.value - y
            total_error += error
            weights = update_weights(weights, error, record.point)
            bias += error

    test_data = PointsFactory.get_n_points(10)

    add_test_data_to_the_plot(green_circle, test_data)

    for record in test_data:
        prediction = np.matrix([record.x, record.y]) * weights + bias
        marker = blue_cross if prediction > 0 else red_cross
        plt.plot(record.x, record.y, marker)

    print("Final weights: " + str(weights))
    print("Final bias: " + str(bias))
    print("Number of epochs: " + str(number_of_epochs))

    def y(x): return (-(bias / weights[1]) / (bias / weights[0])) * x + (-bias / weights[1])

    slope = []
    for i in range(-100, 101):
        slope.append(Point(i, y(i)))

    plt.plot([point.x for point in slope], [float(point.y) for point in slope])
    plt.show()


if __name__ == "__main__":
    main()
