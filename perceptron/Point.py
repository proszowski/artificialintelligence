class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self) -> str:
        return "Point(" + str(self.x) + ", " + str(self.y) + ")"

