import random

from Point import Point


class PointsFactory:
    @staticmethod
    def get_n_points(n):
        points = []
        for i in range(0, n):
            point = Point(random.randint(-100, 100), random.randint(-100, 100))
            points.append(point)
        return points
